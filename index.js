let http = require('http')

let assets = [
	{
		name:"Forklift Cat",
		description:"Caterpillar Forklift",
		stocks:3 
	},

	{
		name:"Backhoe",
		description:"Caterpillar Backhoe",
		stocks: 5
    },
    
    {
		name:"Crane",
		description:"Caterpillar Crane",
		stocks: 2
	},
]


http.createServer((req, res)=> {
	if(req.url === '/assets' && req.method === 'GET') {
		// res.writeHead(200, {'Content-Type' : 'plain/text'})
		// res.end('HELLO')

        let assetsA = JSON.stringify(assets)
        res.writeHead(200, { 'Content-Type': 'application/json' })
        res.end(assetsA)
	}

    if (req.url === '/assets' && req.method === 'POST') {
		
        let reqBody = ""
	
        req.on('data', (data) => {
            reqBody += data
        })

        req.on('end', () => {
            console.log(reqBody)
            // res.writeHead(200, { 'Content-Type': 'plain/text' })
            // res.end('Data received')
            let parsedData = JSON.parse(reqBody)

            assets.push(
                parsedData
            )

            console.log(parsedData)

            let jsonAssets = JSON.stringify(assets)

            res.writeHead(200, { 'Content-Type': 'application/json' })
            res.end(jsonAssets)
        })
    }
        
    if(req.url === '/assets' && req.method ==='PUT') {
		
		let reqBody = ""
	
		req.on('data', (data) => {
			reqBody += data
		})

		req.on('end', () => {
			console.log(reqBody)
			// res.writeHead(200, { 'Content-Type': 'plain/text' })
			// res.end('Data received')
            let parsedData = JSON.parse(reqBody)
            
            assets[parsedData.index].name = parsedData.name

			console.log(parsedData)

			let jsonAssets = JSON.stringify(assets)

			res.writeHead(200, { 'Content-Type': 'application/json' })
			res.end(jsonAssets)
        })
	}


}).listen(8000)

console.log("Server running at 8000")